# crc

 crc8 code code generator
 
usage:

gcc main.c

./a.out > snippet.c

todo:
* make alternativ definitions to store generated code in flash on small controllers
* offer algorithmic variant
* find out more about https://users.ece.cmu.edu/~koopman/crc/crc8.html
* good collection of Information: http://www.miscel.dk/MiscEl/CRCcalculations.html
* create some application code examples, and tests

in short: a c program, creates some c code.
