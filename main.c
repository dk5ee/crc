/*
	one crc8 code code generator ( 0x31 )
	released into public domain
*/
#include <stdio.h>
#include <stdint.h>
int main()
{
  uint8_t i=0;
  uint8_t j=0;
  printf("\nstatic uint8_t const crc8x_table[] =\n{\n  ");
  do {
    uint8_t crc=i;
    for (j = 0; j < 8; j++) {
      if ((crc & 0x80) != 0) {
        crc = (crc << 1) ^ 0x31;
      } else {
        crc <<= 1;
      }
    }
    printf("%3i", crc);
    i++;
    if (i>0) {
      printf(", ");
      if (i % 8==0) {
        printf("\n  ");
      }
    }
  } while (i!=0);
  printf("\n}; \n\n");
  printf("uint8_t crc8x (uint8_t a, uint8_t b) {\n");
  printf("  return crc8x_table[ a ^ b ];\n");
  printf("}\n\n");
  return 0;
}